export class UpdatePost {
    type: string;
    image: string;
    title: string;
    content: string;
}
