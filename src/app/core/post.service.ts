import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  AngularFirestore
} from '@angular/fire/firestore';
import { UpdatePost } from './updatePost';


@Injectable()
export class PostService {

  postsCollection: AngularFirestoreCollection<any>;
  postDocument: AngularFirestoreDocument<any>;

  constructor(private afs: AngularFirestore) {
    this.postsCollection = this.afs.collection('posts');  // , (ref) => ref.orderBy('time', 'desc').limit(5)
  }

  getPosts(): Observable<any[]> {
    return this.postsCollection
      .snapshotChanges()
      .pipe(map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
      );
  }

  getpost(id) {
    this.postDocument = this.afs.doc<any>('posts/' + id);
    return this.postDocument;
  }


  updatepost(post: UpdatePost) {
      this.postDocument.update({
        title: post.title,
        image: post.image,
        type: post.type,
        content: post.content,
      });
  }


  deletepost(id: string) {
    return this.afs.doc<any>(`posts/${id}`).delete();
  }

}
