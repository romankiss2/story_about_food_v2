export interface Post {
    Type: string;
    Image: string;
    Title: string;
    Content: string;
}
