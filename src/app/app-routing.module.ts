import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './admin/auth/auth.component';
import { HomeComponent } from './ui/home/home.component';
import { PostListComponent } from './ui/post-list/post-list.component';
import { AboutComponent } from './ui/about/about.component';
import { PostChangeComponent } from './admin/post-change/post-change.component';
import { AuthGuard } from './admin/auth/auth.guard';
import { PostDetailComponent } from './ui/post-detail/post-detail.component';
import { RecipesComponent } from './ui/recipes/recipes.component';




const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'home/:id', component: PostDetailComponent},
  {path: 'login', component: AuthComponent},
  {path: 'recipes', component: RecipesComponent},

  {path: 'about', component: AboutComponent},
  {path: 'post-change/:id', component: PostChangeComponent, canActivate: [AuthGuard]}

  // {path: "**", component: PageNotFoundComponent},  сторінку не найдено
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  HomeComponent,
  PostDetailComponent,
  PostListComponent,
  AuthComponent,
  AboutComponent,
  RecipesComponent
];

