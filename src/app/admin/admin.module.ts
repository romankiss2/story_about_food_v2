import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdminPostListComponent } from './admin-post-list/admin-post-list.component';
import { AdminUiComponent } from './admin-ui/admin-ui.component';
import { PostChangeComponent } from './post-change/post-change.component';
import { PostFormComponent } from './post-form/post-form.component';
import { AuthService } from './auth/auth.service';
import { AuthComponent } from './auth/auth.component';
import { PostService } from '../core/post.service';
import { AuthGuard } from './auth/auth.guard';
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularEditorModule
  ],
  exports: [
  ],
  declarations: [
    AdminPostListComponent,
    AdminUiComponent,
    PostChangeComponent,
    PostFormComponent,
    AuthComponent,
  ],

  providers: [
    AuthService,
    AuthGuard,
    PostService
  ]
})
export class AdminModule { }
