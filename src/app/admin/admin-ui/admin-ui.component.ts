import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-ui',
  templateUrl: './admin-ui.component.html',
  styleUrls: ['./admin-ui.component.scss']
})
export class AdminUiComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
   ) { }

  ngOnInit() {
  }

}
