import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PostService } from 'src/app/core/post.service';

@Component({
  selector: 'app-admin-post-list',
  templateUrl: './admin-post-list.component.html',
  styleUrls: ['./admin-post-list.component.scss']
})
export class AdminPostListComponent implements OnInit {
  post: any;
  posts: Observable<any[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostService,
   ) { }

  ngOnInit() {
    this.posts = this.postService.getPosts();
  }

  updatePost(id: string) {
    this.router.navigate(['post-change', id ]);
  }

  deletePost(id: string) {
    this.postService.deletepost(id);
}
}
