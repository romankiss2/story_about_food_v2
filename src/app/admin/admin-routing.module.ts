import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminUiComponent } from './admin-ui/admin-ui.component';
import { PostChangeComponent } from './post-change/post-change.component';
import { PostFormComponent } from './post-form/post-form.component';
import { AdminPostListComponent } from './admin-post-list/admin-post-list.component';
import { AuthGuard } from './auth/auth.guard';
import { PostDetailComponent } from '../ui/post-detail/post-detail.component';


const adminRoutes: Routes = [

  {path: 'admin', component: AdminUiComponent, canActivate: [AuthGuard], children: [
    {path: 'postForm', component: PostFormComponent, outlet: 'cabinet'},
    {path: 'adminPostList', component: AdminPostListComponent, outlet: 'cabinet'},
  ]},


];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes),
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
export const routingComponents = [
  AdminUiComponent,
  PostChangeComponent,
  PostDetailComponent,
  AdminPostListComponent
];

