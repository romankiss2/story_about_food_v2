import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from 'src/app/core/post.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';


@Component({
  selector: 'app-post-change',
  templateUrl: './post-change.component.html',
  styleUrls: ['./post-change.component.scss']
})
export class PostChangeComponent implements OnInit {

  public editForm: FormGroup;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    private postServ: PostService,
    private fb: FormBuilder,
    private location: Location,
    ) { }

  ngOnInit() {
    this.updatePostData();
    const id = this.actRoute.snapshot.paramMap.get('id');
    this.postServ.getpost(id).valueChanges().subscribe(data => {
    this.editForm.setValue(data);
    });
  }


  get type() {
    return this.editForm.get('type');
  }

  get title() {
    return this.editForm.get('title');
  }

  get image() {
    return this.editForm.get('image');
  }

  get content() {
    return this.editForm.get('content');
  }



  updatePostData() {
    this.editForm = this.fb.group({
      type: ['', Validators.required],
      title: ['', Validators.required],
      image: ['', Validators.required],
      content: ['', Validators.required],
    });
  }
  goBack() {
    this.location.back();
  }
  updateForm() {
    this.postServ.updatepost(this.editForm.value);
    this.goBack();
  }

}
