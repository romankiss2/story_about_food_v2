import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from 'src/app/core/form';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {
  public postForm: FormGroup;
  post: Post;
  private image: string;
  private event: any;

  public imagePath;
  imgURL: any;
  public message: string;

  downloadURL: Observable<string>;
  private postsCollection: AngularFirestoreCollection<Post>;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '400px',
    showToolbar: true,
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
  };


  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private afs: AngularFirestore,
    private fb: FormBuilder,
    private storage: AngularFireStorage,
  ) { }

  ngOnInit() {
    this.postsCollection = this.afs.collection<Post>('posts/');

    this.postForm = this.fb.group({
      title: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      content: new FormControl('', Validators.required)
    });
    this.postForm.valueChanges.subscribe(inputData => {
      this.post.Title = this.postForm.controls.title.value;
      this.post.Image = this.image;
      this.post.Type = this.postForm.controls.type.value;
      this.post.Content = this.postForm.controls.content.value;
    });
  }


  uploadImage(event, files) {
    this.event = event;
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      this.imgURL = reader.result;
    };
  }

  async onSubmit() {
    if (this.postForm.valid) {
      if (this.event == null) {
        return alert('Картинка відсутня!');
      }

      const file = this.event.target.files[0];
      const filePath = `post/${file.name}`;
      if (file.type.split('/')[0] !== 'image') {
        return alert('Тільки картинки!');
      } else {
        const task = this.storage.upload(filePath, file);
        const fileRef = this.storage.ref(filePath);
        task.then(tsk => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            this.image = url;
            this.post = this.prepareSavePost();
            this.postsCollection.add(this.post);
            this.postForm.reset();
            this.imgURL = null;
            this.event = null;
            alert('Успішно надіслано!');
          });
        }
        );

      }
    } else {
      return alert('Заповніть всі поля!');
    }

}


  prepareSavePost(): Post {
    const formModel = this.postForm.value;
    formModel.image = this.image;
    return {...formModel};
  }
}

