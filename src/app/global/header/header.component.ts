import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/admin/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }
  openNav() {
    document.getElementById('mySidenav').style.width = '20vw';
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
  }

}
