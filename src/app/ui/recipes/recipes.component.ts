import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit {

  typeFilter: BehaviorSubject<string|null>;
  posts: Observable<{}[]>;
  content: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private afs: AngularFirestore
  ) { }

  ngOnInit() {
    this.typeFilter = new BehaviorSubject('');
    this.posts = combineLatest(
      this.typeFilter
    ).pipe(
      switchMap(([type]) =>
        this.afs.collection('posts', ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
          if (type) { query = query.where('type', '==', type); }
          return query;
        }).valueChanges()
      )
    );
  }
  filterByType(type: string|null) {
    this.typeFilter.next(type);
  }
  openPost(id: string) {
    this.router.navigate(['/home' + id]);
  }


}
