import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostService } from '../core/post.service';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { RecipesComponent } from './recipes/recipes.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    ScrollingModule,
  ],
  exports: [
    AboutComponent,
    HomeComponent,
    RecipesComponent,
    LoadingSpinnerComponent,
    PostDetailComponent,
    PostListComponent,
    ],
  declarations: [
    AboutComponent,
    HomeComponent,
    LoadingSpinnerComponent,
    PostDetailComponent,
    PostListComponent,
    RecipesComponent,
  ],
  providers: [PostService]

})
export class UiModule { }
