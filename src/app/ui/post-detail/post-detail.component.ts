import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { PostService } from 'src/app/core/post.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  post: any;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute) { }

  ngOnInit()  {
    this.route.paramMap.subscribe(params => {
    this.postService.getpost(params.get('id'));
      });
    this.post = this.postService.postDocument.valueChanges();
  }

}
